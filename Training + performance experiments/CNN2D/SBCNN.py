from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, MaxPooling2D, Conv2D, BatchNormalization, Input
from tensorflow.keras import regularizers
from qkeras import *
from qkeras.estimate import print_qstats
from qkeras.utils import model_quantize
from qkeras.utils import quantized_model_dump

def CustomSBCNN_Model(bands, frames, num_channels):

    input_layer = Input(shape=(bands,frames,num_channels))

    net = Conv2D(24, (5,5), padding='same', activation='relu')(input_layer)
    net = BatchNormalization()(net)
    net = MaxPooling2D(pool_size=(3,3))(net)

    net = Conv2D(48, (5, 5), padding='same', activation='relu')(net)
    net = BatchNormalization()(net)
    net = MaxPooling2D(pool_size=(3,3))(net)

    net = Conv2D(48, (5, 5), padding='valid', activation='relu')(net)
    net = BatchNormalization()(net)
    net = MaxPooling2D(pool_size=(3,3))(net)

    net = Flatten()(net)
    net = Dropout(0.5)(net)
    net = Dense(64, kernel_regularizer=regularizers.l2(0.001), activation='relu')(net)
    net = BatchNormalization()(net)
    net = Dropout(0.5)(net)
    net = Dense(10, kernel_regularizer=regularizers.l2(0.001))(net)
    outputLayer = Activation('softmax')(net)

    model = Model(inputs=input_layer, outputs=outputLayer, name="SBCNN")

    return model

def QCustomSBCNN_Model(bands, frames, num_channels):

    bits=4
    bits_2=0
    alpha=1
    input_layer = Input(shape=(bands,frames,num_channels))

    net = QConv2D(24, (5,5), padding='same', activation='relu', kernel_quantizer=quantized_bits(bits,bits_2,alpha), bias_quantizer=quantized_bits(bits,bits_2,alpha))(input_layer)
    net = QBatchNormalization()(net)
    net = MaxPooling2D(pool_size=(3,3))(net)

    net = QConv2D(48, (5, 5), padding='same', activation='relu', kernel_quantizer=quantized_bits(bits,bits_2,alpha), bias_quantizer=quantized_bits(bits,bits_2,alpha))(net)
    net = QBatchNormalization()(net)
    net = MaxPooling2D(pool_size=(3,3))(net)

    net = QConv2D(48, (5, 5), padding='valid', activation='relu', kernel_quantizer=quantized_bits(bits,bits_2,alpha), bias_quantizer=quantized_bits(bits,bits_2,alpha))(net)
    net = QBatchNormalization()(net)
    net = MaxPooling2D(pool_size=(3,3))(net)

    net = Flatten()(net)
    net = Dropout(0.5)(net)
    net = QDense(64, kernel_regularizer=regularizers.l2(0.001), activation='relu', kernel_quantizer=quantized_bits(bits,bits_2,alpha), bias_quantizer=quantized_bits(bits,bits_2,alpha))(net)
    net = QBatchNormalization()(net)
    net = Dropout(0.5)(net)
    net = QDense(10, kernel_regularizer=regularizers.l2(0.001), kernel_quantizer=quantized_bits(bits,bits_2,alpha), bias_quantizer=quantized_bits(bits,bits_2,alpha))(net)
    outputLayer = Activation('softmax')(net)

    model = Model(inputs=input_layer, outputs=outputLayer, name="SBCNN")

    return model
