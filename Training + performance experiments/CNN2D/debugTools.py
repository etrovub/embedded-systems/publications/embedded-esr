import matplotlib.pyplot as plt
import librosa.display
import numpy as np

def showSpectrogram(spectrogram, SR, name=""):
    '''
    This is a debug function that shows the spectrogram.
    WARNING: librosa displays spectrograms upside down (rows are going from bottom to top).
    If the spectrogram is from an image file, use showSpectrogramImage.
    '''
    fig, ax = plt.subplots()
    img = librosa.display.specshow(spectrogram, x_axis='time', y_axis='mel', sr=SR, fmax=SR//2, ax=ax, cmap='plasma')
    fig.colorbar(img, ax=ax, format='%+2.0f dB')
    ax.set(title='Mel-frequency spectrogram' if not name else f'Mel-frequency spectrogram of {name}')
    plt.show()

def showSpectrogramImage(spectrogram: np.ndarray):
    if(spectrogram.ndim ==2):
        plt.imshow(spectrogram, cmap="plasma")
    elif(min(spectrogram.shape)==1):
        plt.imshow(spectrogram, cmap='gray')
    else:
        plt.imshow(spectrogram)
    plt.show()