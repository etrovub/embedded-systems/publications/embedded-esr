# train, evaluate and save trained keras model
quantize() {
  python quantize.py \
    --keras_hdf5   ${KERAS}/${K_MODEL} \
    --test_fold    ${testFold} \
    --quant_model  ${QUANT}/${Q_MODEL}
}

echo "-----------------------------------------"
echo "QUANTIZATION STARTED"
echo "-----------------------------------------"
rm -rf ${QUANT}
mkdir -p ${QUANT}
quantize 2>&1 | tee ${LOG}/${QUANT_LOG}

echo "-----------------------------------------"
echo "QUANTIZATION FINISHED"
echo "-----------------------------------------"