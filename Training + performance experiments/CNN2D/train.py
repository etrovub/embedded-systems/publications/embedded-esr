import os
# shut up TF
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


import sys
import numpy as np

import tensorflow as tf
from tensorflow.keras.optimizers import SGD, Adam
from tensorflow.keras.callbacks import TensorBoard,ModelCheckpoint,LearningRateScheduler

from config import *
from loadDataset import loadUrbansound8k
from SBCNN import CustomSBCNN_Model
#import debugTools

def train(conf: configuration):
    '''
    Setup
    '''
    # Seed RNGs 
    np.random.seed(1)
    tf.random.set_seed(1)

    # Report start loading process
    print("Loading data")

    # Load the dataset
    (X_train, y_train), (x_val, y_val), (x_test, y_test) = loadUrbansound8k(conf.testFold, conf.valFold)
    # (X_train, y_train), (x_val, y_val), (x_test, y_test) = loadUrbansound8kDataset_test(conf.testFold, conf.valFold)

    print("Generating model")

    # Create the model
    model = CustomSBCNN_Model(conf.inputHeight, conf.inputWidth, conf.inputChan)

    # prints a layer-by-layer summary of the network
    print('\n'+DIVIDER)
    print(' Model Summary')
    print(DIVIDER)
    print(model.summary())
    print("Model Inputs: {ips}".format(ips=(model.inputs)))
    print("Model Outputs: {ops}".format(ops=(model.outputs)))

    '''
    Callbacks
    '''
    # Saving the model
    chkpt_call = ModelCheckpoint(filepath=conf.keras_hdf5,
                                 monitor='val_accuracy',
                                 verbose=1,
                                 save_best_only=True)

    # tb_call = TensorBoard(log_dir=tboard,
    #                       batch_size=BATCH_SIZE,
    #                       update_freq='epoch')


    # Create the callback list
    # callbacks_list = [chkpt_call, tb_call]
    callbacks_list = [chkpt_call]


    '''
    Training metrics
    '''  
    metr = ["accuracy"]

    '''
    Training optimizer
    '''
    # Set the optimizer
    optimizer = Adam(learning_rate= 0.001)

    # Compile the model
    model.compile(loss='categorical_crossentropy',
                optimizer=optimizer,
                metrics=metr)

    # Train the model
    model.fit(X_train, y_train,
              batch_size=BATCH_SIZE,
              epochs=EPOCHS,
              verbose=1,
              shuffle=False,
              validation_data=(x_val, y_val),
              callbacks=callbacks_list)

    '''
    Evaluation
    '''

    print('\n'+DIVIDER)
    print(' Evaluate model accuracy with test set..')
    print(DIVIDER)
    scores = model.evaluate(x=x_test,y=y_test,batch_size=BATCH_SIZE, verbose=1)
    print ('Evaluation Loss    : ', scores[0])
    print ('Evaluation Accuracy: ', scores[1])


def main():
    # Load the configuration
    conf = configuration()

    # Print information for the log file
    print('\n'+DIVIDER)
    print('Keras version      : ',tf.keras.__version__)
    print('TensorFlow version : ',tf.__version__)
    print(sys.version)
    print(DIVIDER)
    conf.printTrainSettings()
    print(DIVIDER)

    # train the model
    train(conf)

if __name__ == '__main__':
    main()