import os

from tensorflow import keras
# shut up TF (completely)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


import sys
import numpy as np

import tensorflow as tf

from config import *
from loadDataset import loadUrbansound8k

def main():
    # Load the configuration
    conf = configuration()
    batchsize = 30

    # Seed RNGs 
    np.random.seed(1)
    tf.random.set_seed(1)

    # Get the test data
    (_), (_), (x_test, y_test) = loadUrbansound8k(conf.testFold, conf.valFold)
    model = keras.models.load_model(conf.keras_hdf5)
    
    print('\n'+DIVIDER)
    print(' Evaluate model accuracy with test set and best model.')
    print(DIVIDER)
    scores = model.evaluate(x=x_test,y=y_test,batch_size=batchsize, verbose=1)
    print ('Evaluation Loss    : ', scores[0])
    print ('Evaluation Accuracy: ', scores[1])


if __name__ == '__main__':
    main()