import xir

filePath = "./build/fold10/compile/model/SBCNN.xmodel"
fileName = "graph.png"

g = xir.Graph.deserialize(filePath)
g.save_as_image(fileName, "png")

