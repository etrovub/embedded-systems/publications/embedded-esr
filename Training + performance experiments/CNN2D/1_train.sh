#!/bin/bash

# train, evaluate and save trained keras model
train() {
  python train.py \
    --input_height ${INPUT_HEIGHT} \
    --input_width  ${INPUT_WIDTH} \
    --input_chan   ${INPUT_CHANNEL} \
    --tboard       ${TB_LOG} \
    --keras_hdf5   ${KERAS}/${K_MODEL} \
    --val_fold     ${valFold} \
    --test_fold    ${testFold}
}

test() {
  python testModel.py \
    --input_height ${INPUT_HEIGHT} \
    --input_width  ${INPUT_WIDTH} \
    --input_chan   ${INPUT_CHANNEL} \
    --tboard       ${TB_LOG} \
    --keras_hdf5   ${KERAS}/${K_MODEL} \
    --val_fold     ${valFold} \
    --test_fold    ${testFold}
}

echo "-----------------------------------------"
echo "TRAINING STARTED"
echo "-----------------------------------------"

rm -rf ${KERAS}
mkdir -p ${KERAS}
train 2>&1 | tee ${LOG}/${TRAIN_LOG}
test 2>&1 | tee ${LOG}/${TEST_LOG}

echo "-----------------------------------------"
echo "TRAINING FINISHED"
echo "-----------------------------------------"
