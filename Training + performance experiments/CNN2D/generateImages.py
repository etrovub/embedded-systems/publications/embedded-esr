import os
import os.path
import shutil
import pandas as pd

from config import *


# List of class names, used to name the folders
classes = ['air_conditioner','car_horn','children_playing','dog_bark','drilling','engine_idling','gun_shot','jackhammer','siren','street_music'] 

def generateImages(conf: configuration):
    # Get the main output folder for the spectrograms
    spectrogramFolder = os.path.join(conf.outputDir, "spectrograms")

    # Create the folders to save the spectrograms
    createOutputFolders(spectrogramFolder)

    # Load the meta data file
    df = pd.read_csv(os.path.join(SPEC_FILE_PATH, SPEC_META_CSV))

    # If only the tet fold should be exported
    if(not conf.exportAll):
        # Create a filter to filter out the test fold
        df_filter = df["fold"] == conf.testFold

        # filter the dataframe
        df = df[df_filter]

    # Copy each file to the target folder
    for originalFilePath, fileName, classID in zip(df['filePath'], df['fileName'], df['classID']):
        copyImageFile(originalFilePath, fileName, classID, spectrogramFolder)
    
    # Copy the target application file
    shutil.copyfile("app_mt_MWE.py", os.path.join(conf.outputDir, "app_mt_MWE.py"))

  
def copyImageFile(originalFilePath, fileName, classID, spectrogramFolder):

    # Get the path to the target file
    targetFilePath = os.path.join(spectrogramFolder, classes[classID], fileName)

    # Copy the file
    shutil.copyfile(originalFilePath, targetFilePath)


def createOutputFolders(spectrogramFolder):
    # If the output folder exists...
    if(os.path.isdir(spectrogramFolder)):
        # Delete the output folder and its content
        shutil.rmtree(spectrogramFolder)

    # Create the output folder
    os.mkdir(spectrogramFolder)

    # For each label...
    for i in classes:
        # Create the output folder for this label
        os.mkdir(os.path.join(spectrogramFolder, f"{i}"))


def main():
    # Load the configuration
    conf = configuration()

    # Print the settings
    conf.printExportSettings()

    # Generate the images
    generateImages(conf)



if __name__ == '__main__':
    main()